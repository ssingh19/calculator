package edu.towson.cosc431.SundeepSingh.Calculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import net.objecthunter.exp4j.ExpressionBuilder
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        zerobtn.setOnClickListener { displayInput("0", true) }
        onebtn.setOnClickListener { displayInput("1", true)}
        twobtn.setOnClickListener { displayInput("2", true)}
        threebtn.setOnClickListener { displayInput("3", true)}
        fourbtn.setOnClickListener { displayInput("4", true) }
        fivebtn.setOnClickListener { displayInput("5", true) }
        sixbtn.setOnClickListener { displayInput("6", true) }
        sevenbtn.setOnClickListener { displayInput("7", true)}
        eightbtn.setOnClickListener { displayInput("8", true) }
        ninebtn.setOnClickListener { displayInput("9", true) }
        decbtn.setOnClickListener { displayInput(".", true) }
        addbtn.setOnClickListener { displayInput("+", false)}
        subbtn.setOnClickListener { displayInput("-",  false) }
        multiplybtn.setOnClickListener { displayInput("*", false)}
        divbtn.setOnClickListener { displayInput("/",  false)}

        clearbtn.setOnClickListener {
            operdisplay.text = ""
            display.text = ""
        }

        equalbtn.setOnClickListener {
            try {
                val exp = ExpressionBuilder(operdisplay.text.toString()).build()
                val result = exp.evaluate()
                val result2 = result.toLong()
                if(result == result2.toDouble())
                {
                    display.text = result2.toString()
                }else
                {
                    display.text = result.toString()
                }

            }catch (e:Exception){
                display.text = "Exception Error"
            }

        }

    }

    fun displayInput(string:String, isempty:Boolean){
        if (display.text.isNotEmpty()){
            operdisplay.text = ""
        }

        if (isempty)
        {
            display.text = ""
            operdisplay.append(string)
        }
        else{
            operdisplay.append(display.text)
            operdisplay.append(string)
            display.text = ""
        }
    }


}
